angular.module("resolve", ["ui.router"])
    .config(function($stateProvider) {
        $stateProvider
            .state("resolve", {
                url: '/resolve',
                templateUrl: "/app/resolve/resolve.html",
                controller: "ResolveController",
                controllerAs: "rc",
                resolve: {
                    myNames: function ($http, $timeout) {
                        return $timeout(function() {
                            return $http.get("data/namers.json").then(function (response) {
                                return response.data;
                            }, function(error) {
                                console.log("myNames");
                                return ['we got an error']
                            })
                        }, 2000);
                    },
                    anotherPromise: function ($q) {
                        console.log("anotherPromise");
                        return $q.when("Success");
                    }
                }
            })
    })
    .controller("ResolveController", function(myNames) {
        var vm = this;
        this.names = myNames;
    });