angular.module("demoApp", [
        "ui.router",
        "nameDisplay",
        "watchThis",
        "promises",
        "resolve",
        "directives"
    ])
    .config(function($urlRouterProvider) {
        $urlRouterProvider
            .otherwise("/name");
    });
