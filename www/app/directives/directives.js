angular.module("directives", ["ui.router"])
    .config(function($stateProvider) {
        $stateProvider
            .state("directives", {
                url: '/directives',
                templateUrl: "/app/directives/directives.html",
                controller: "DirectivesController",
                controllerAs: "dc"
            })
    })
    .controller("DirectivesController", function ($http) {
        var vm = this;
        this.format = 'M/d/yy h:mm:ss a';
        this.addresses = [];
        $http.get("app/directives/addresses.json").then(function(response) {
            vm.addresses = response.data;
        })

        this.addAddress = function addAddress() {
            vm.addresses.push({});
        }
    })
    .directive("jrbName", function () {
        return {
            template: "My name is John<br>"
        }
    })
    .directive("jrbAddress", function () {
        return {
            restrict: "EA",
            templateUrl: "app/directives/address.html",
            scope: {
                jrbAddress: "="
            }
        }
    })
    .directive("jrbPanel", function () {
        return {
            templateUrl: "app/directives/panel.html",
            transclude: true
        }
    })
    .directive('myCurrentTime', function($interval, dateFilter) {
        function setup(scope, element, attrs) {
            var format,
                timeoutId;

            function updateTime() {
                console.log("update");
                element.text(dateFilter(new Date(), format));
            }

            scope.$watch(attrs.myCurrentTime, function(value) {
                format = value;
                updateTime();
            });

            // start the UI update process; save the timeoutId for canceling
            timeoutId = $interval(function() {
                updateTime(); // update DOM
            }, 1000);

            element.on('$destroy', function() {
                $interval.cancel(timeoutId);
            });
        };

        return {
            link: setup,
            controller: function () {

            }
        };
    });