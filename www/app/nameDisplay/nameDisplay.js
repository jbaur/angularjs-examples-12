angular.module("nameDisplay", ["ui.router"])
    .config(function($stateProvider) {
        $stateProvider
            .state("name", {
                url: '/name',
                templateUrl: "/app/nameDisplay/nameDisplay.html",
                controller: "MainController",
                controllerAs: "mc"
            })
        .state('name.details', {
            url: '/details',
            template: "This is the child state! <a ui-sref='name'>Go Back</a>"
        })
    })
    .controller("MainController", function() {
        var vm = this;
        this.getData = function(){
            return vm.name;
        }
        this.name = {
            first: "John"
        };
    })
    .controller("SecondController", function() {

    });
