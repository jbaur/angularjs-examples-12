angular.module("watchThis", ["ui.router"])
    .config(function($stateProvider) {
        $stateProvider
            .state("watch", {
                url: '/watch',
                templateUrl: "/app/watchThis/watchThis.html",
                controller: "WatchController"
            })
    })
    .controller("WatchController", function($scope) {
        $scope.$watch("counter", function(n) {
            $scope.boxes = [];
            for(var i = 0; i < parseInt(n); i++) {
                $scope.boxes.push({i: i})
            }
        });

        $scope.name = {
            first: "John",
            last: "Baur"
        };
        $scope.fullName = function fullName() {
            return $scope.name.first + " " + $scope.name.last;
        };

        $scope.$watch("name.first", function(n, o) {
            console.log("new:" + n + ", old:" + o);
        })
        $scope.$watch("name", function(n, o) {
            console.log("new name:" + n + ", old name:" + o);
        }, true)
    });
