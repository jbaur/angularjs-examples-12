angular.module("promises", ['ui.router'])
    .config(function ($stateProvider) {
        $stateProvider.state("promises", {
            url: "/promises",
            controller: "PromiseController",
            controllerAs: "pc",
            templateUrl: "app/promises/promises.html"
        });
    })
    .factory('promiseFactory', function ($http, $q) {
        return {
            getData: function () {
                return $http.get('app/data.json').then(function(response) {
                    console.log(response);
                    return response.data;
                })
                .then(function (data) {
                    return data.number + 3;
                });
            },
            getBadData: function () {
                return $http.get('doesnotexist.json').then(function (response) {
                    return response.data;
                }, function (err) {
                    console.log("I'm a failure, here's why: " + err.status);
                    return $q.reject(err);
                });
            }
        }
    })
    .controller("PromiseController", function (promiseFactory, $q) {
        var vm = this;
        var p0 = promiseFactory.getBadData().then(function (data) {
            console.log("My boss actually did his job!");
        }, function (err) {
            vm.warning = "We failed to get data!";
            console.log("I am taking charge of this error!")
        });
        var p1 = promiseFactory.getData();
        var p2 = promiseFactory.getData();
        var p4 = p2.then(function (number) {
            return number + 3;
        });
        var p5 = p2.then(function (number) {
            console.log("for this one, I want to add 9: " + (number + 9));
        });
        var p3 = $q.all([p1, p4]);

        p3.then(function (arr) {
            console.log("p1 got: " + arr[0]);
            console.log("p2 got: " + arr[1]);
        });

        p1.then(function (number) {
            console.log(number);
            vm.number = number

        });
        $q.when("This is some data").then(function (data) {
            console.log(data);
        });
        var d = $q.defer();
        vm.resolve = function () {
            d.resolve("Happy Path");
        }
        vm.reject = function () {
            d.reject("Sad Path");
        }
        var p6 = d.promise;
        p6.catch(function (err) {
            console.log("We handled an error");
            throw "PANIC! Everything is broken";
        }).then(function () {
            console.log("We successfully failed");
        }).catch(function (err) {
            console.log(err);
        });
        p6.then(function (message) {
            console.log(message);
            p6.then(function () {
                console.log("Does this execute?");
            })
        }, function (err) {
            console.log(err);
        }).then(function () {
            console.log("happy")
        }, function () {
            console.log("sad")
        });

        $q.all([p6, p1]).then(function (data) {
            console.log(data);
        }, function (err) {
            console.log("The all error was: " + err);
        });

    });
