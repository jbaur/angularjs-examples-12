console.log("Starting Login tests");
describe('Login E2E test', function () {
    // Since angular is not on the login page, we need to use more primative tooling to test it
    it('logs in', function () {
        browser.driver.get('localhost:8080/login');
        var username = browser.driver.findElement(by.id('username'));
        username.sendKeys("user");
        var password = browser.driver.findElement(by.id('password'));
        password.sendKeys("secret");
        var login = browser.driver.findElement(by.css('.login'));
        login.click();
        expect(browser.driver.getCurrentUrl()).toBe("http://localhost:8080/");
    });
});
