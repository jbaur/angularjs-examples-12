describe('The application', function () {
    beforeEach(module('promises'));

    describe("has a service", function () {
        var promiseFactory, $httpBackend,
            data = {
                number: 3
            };
        beforeEach(inject(function ($injector) {
            promiseFactory = $injector.get('promiseFactory');
            $httpBackend = $injector.get('$httpBackend');
        }));
        it('that can get data', function () {
            $httpBackend.expectGET('app/data.json').respond(data);
            promiseFactory.getData().then(function (d) {
                expect(d).toBe(6);
            });
            $httpBackend.flush();
        });
    });
    describe("has a controller", function () {
        var $q, $controller, mockPromiseFactory;
        beforeEach(inject(function ($injector) {
            $q = $injector.get('$q');
            $controller = $injector.get('$controller');
            mockPromiseFactory = {
                getData: function () {
                    return $q.when(3);
                },
                getBadData: function () {
                    return $q.reject('This is a mock error');
                }
            }
        }));
        it('sets up the scope properly', function () {
            var scope = {};
            $controller('PromiseController as pc',
                {promiseFactory: mockPromiseFactory, $scope: scope});
            expect(scope.pc.reject).toBeDefined();
            expect(scope.pc.resolve).toBeDefined();
            //Doesn't work because asynch
            //expect(scope.pc.number).toBe(6);
        })
    });
})
